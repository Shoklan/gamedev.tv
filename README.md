# Gamedev.tv Timeline
## Place to store lecture notes from Gamedev Classes.

# Gamedev.tv Timeline

| Class Name                                     | Directory                                  | Date Completed | Reviewed |
| -----------------------------------------------| -------------------------------------------| ---------------| ---------|
| Unity Multiplayer: Intermediate C# Networking  | [/UnityMultiplayerNetwork]                 | {}             |          |
| Unreal 4.22 C++ Developer: - Make Video Games  | [/UnrealLearnCppVideoGame]                 | {2021/05/14}   |          |
| Blender Environment Artist: Create 3D Worlds   | [/BlenderEnvironment3DWords]               | {2021/07/16}   |          |
| Blender Creator: 3D Modeling for Beginners     | [/Blender3DModelingBeginners]              | {2021/07/16}   |          |
| Unreal Multiplayer Master: Video Game Dev C++  | [/UnrealMultiplayerMasterCpp]              | {2021/08/09}   |          |
| C++ Fundamentals: Programming For Beginners    | [/CPPFundamentalsForBeginners]             | {}             |          |
| Low Poly Landscapes - Blender Bite Sized       | [/BiteSizedLowPolyBlender]                 | {2021/08/26}   |          |
| Low Poly Characters in Blender                 | [/LowPolyCharacterInBlender]               | {2021/08/27}   |          |
| Create Battle Royale game using UE4 Blueprints | [/CreateBattleRoyale]                      | {}             |          |


# Game Developer Courese Timeline:

| Class Name                                     | Directory                                  | Date Completed | Reviewed |
| -----------------------------------------------| -------------------------------------------| ---------------| ---------|
| UE4 Mastery: Create Multiplayer Games with C++ | [/UE4MultiplayerMastery]                   |                |          |
| Unreal Engine C++ The Ultimate Shooter Course  | [/UEUltimateShooter]                       |                |          |



| Key |    Meaning   |
| --- | ------------ |
| A   |  Audit       |
| O   |  Organize.   |
| F   |  Freeze.     |
| R   |  Reviewed    |
