# Low Poly Landscapes - Blender Bite Sized Course
## Grant Abbitt

# Beginning/Interface
- The name **Low Poly** comes from games with *low poly* or low face count.
- While it is less of a concern today, it still matters for performance.
- Since then, it has been a popular artistic style and a challenge for artists.
- // Skipped the Download and interface lectures.
- The movement transform is in relation to the viewport.


# Basic Modelling
- The first step when working is to first **Block Out** the shapes for the scene.
- It is must safer to add details after in case you need to change things around later.
- You can use `TAB` to go into **Edit Mode**.
- You can use the numbers - *not* the numberpad - `1`, `2`, and `3` to switch between vertexes, edges and faces.
- Be careful about vertex selection since you need to be able to see the vertex to select it.
- You can work around this by using *Wireframe Mode*.
- You can use `m` to merge two points together.
    * In my version this is actually `ALT` + `m`.
- **Always remember to add meshes in Object Mode**.
- If you want to access a local transform then you can double tab the axis that you want.
- `ALT` will generally undo a transformation.
- You can use `ALT` + `LEFT CLICK` to select a *Loop Cut* which has been done.
- **Dissolving Edges** will undo a loop cut since it removes the edges but keeps the faces.
- You can use the mouse wheel to increase the number of loop cuts as well on a surface.
- When you are in **X-Ray MOde**, you need to be closer to the dot representing the face while selecting:
![Select Carefully in Xray] (images/xray-select-face-dot.png)
- You can use a **Mirror Modifier** in case you want symmetry in the mesh.
- The mirror is based on the origin point which you can mess with:
![Mirror Modifier Reference Point](images/mirror-origin-point-reference.png)
- You will only move your object with its origin point in *Object Mode*.
- The *mirror* is done based on the *Local Coordinates* and not the *Global Coordinates*.
- You can also change the mirror reference to another object as well.
- You can also turn on **Clipping** which will prevent the mirror from passing through the source object.
- You can use `/` to focus and hide other objects in your scene.
- If you want to split a face arbitrarily then you can use the **Knife Toll** - with `k`.
- This still works best when you cut between vertexes.
- You can use the **Solidify Modifier** to add thickness to planes.
- The order of the modifiers in the pane **does matter**.
- Beware that modifiers will be in relation to the scale of the object.
- You can access the **Apply Menu** using `CTRL + a` to apply your transforms.
- You can also apply the modifier to the mesh which will remove the modifier from the pane and convert the mesh into the result.
- The keyboard shortcut for *X-ray Mode* is `CTRL + z` and is a toggle.
- If you apply a modifier to a model, then make sure you're done since you can't go back.
- You can use `ALT` + `s` to scale based on the normals for a mesh object.
- You can hide objects with `ALT` + `h`.
- **N-Gons** are best when they're on flat surfaces.
- While using the *Knife Tool*, you can press `e` to start a new cut.
- When you do an *extrude* and then `RIGHT CLK` then it will keep the extrude vertices but cancel the movement.


# Landscape
- You can use `f` while in **Sculpter Mode** to control the brush size.
- You can use `SHIFT` + `f` to change the strength
- You can use the **Decimate Modifier** to reduce the amount of faces.
- When you have an object selected in the viewport, when you highlight over the heirarchy and press `.` then it will jump to that object.
- You can use the **Lasso Select** by pressing `CTRL` + `RIGHT CLK` and then drawing around objects.
- It grabs things based on the object's center.
- You can cut whole into meshes using a **Boolean Modifier**.


# Color:
- You can change the color of something by clicking on it in the **Shading Pane** and then simply changing the color.
- YOu can put multiple textues on the same material using *Texture Slots*.
- You will want to go into edit mode and select the faces now in the Viewport.
- Make sure you assign those spaces to *Slot 2*.
- You can modify the view of the world around the objects using the **Shader Pane's World Option**:
![Shader Modify World](images/shader-modify-world.png)
- You will want to add an **Environment Texture** to the World.
- It is a classic film trick to use blue light to maintain the atmosphere but with better lighting.
- **Compositing** is a word that means bringing all the elements in a scene together.


# Research:
- Bevel?

# Reference:
- [PolyHaven](https://www.polyhaven.com)
