#include <iostream>
#include <ctime>


void PrintIntroduction(int Difficulty){
   std::cout << "\n\nYou are a secret agent breakigng into a Level " << Difficulty;
   std::cout << "secure server room...\nEnter the correct code to continue...\n\n";
}


bool PlayGame(int Difficulty){

   PrintIntroduction(Difficulty)

   const int CodeA = rand() % Difficulty + 1;
   const int CodeB = rand() % Difficulty + 1;
   const int CodeC = rand() % Difficulty + 1;

   const int CodeSum = CodeA + CodeB + CodeC;
   const int CodeProduct = CodeA * CodeB * CodeC;

   /// Print CodeSum and CodeProduct to the terminal
   std::cout << std::endl;
   std::cout << "+ There are 3 numbers in the code";
   std::cout << "\n+ The codes add up to: " << CodeSum;
   std::cout << "\n+ The codes multiply to give: " << CodeProduct << std::endl;

   int GuessA, GuessB, GuessC;
   std::cin >> GuessA >> GuessB >> GuessC;

   const int GuessSum = GuessA + GuessB + GuessC;
   const int GuessProduct = GuessA * GuessB * GuessC;


   if ( GuessSum == CodeSum && GuessProduct == CodeProduct){
      std::cout << "WOW - You're a Master hacker!\n";
      return true
   }

   else{
      std::cout << "You Lose!";
      return false
   }
}

int main(){

   int LevelDifficulty = 1;
   int const MaxDifficulty = 10;
   while( LevelDifficulty <= MaxDifficulty ){

      // Play the Game
      bool bLevelComplete = PlayGame(LevelDifficulty);
      std::cin.clear();  // clears and errors.
      std::cin.ignore(); // discard the input buffer.

      if (bLevelComplete) {
         // increase the level of difficulty
         ++LevelDifficulty;
      }
   }

   return 0; //Exit with no error code
}
